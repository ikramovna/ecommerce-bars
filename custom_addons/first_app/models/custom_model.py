import requests
import base64
import io
import xlrd
from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

class Brand(models.Model):
    _name = 'custom_brand'
    _description = 'Product Brand'

    name = fields.Char(string='Name', required=True)
    logo = fields.Binary(string='Logo')
    description = fields.Text(string='Description')


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    brand_id = fields.Many2one('custom_brand', string='Brand')

    min_orders = fields.Integer(string='Minimum Orders', default=0)

    @api.model
    def create(self, vals):
        product = super(ProductTemplate, self).create(vals)
        if product.min_orders < 0:
            raise ValidationError("Minimum orders cannot be negative.")
        return product

    @api.constrains('min_orders')
    def _check_min_orders(self):
        for product in self:
            if product.min_orders < 0:
                raise ValidationError("Minimum orders cannot be negative.")

    def check_minimum_orders(self, quantity):
        if self.min_orders and quantity < self.min_orders:
            raise ValidationError("Minimum orders requirement not met for this product.")
        return True

    @api.model
    def import_products_from_excel(self, excel_file):
        # Check if file is provided
        if not excel_file:
            raise ValidationError(_('Please provide an Excel file.'))

        # Read the Excel file
        excel_data = io.BytesIO(excel_file)
        workbook = xlrd.open_workbook(file_contents=excel_data)
        sheet = workbook.sheet_by_index(0)

        # Iterate through rows to create/update products
        for row_index in range(1, sheet.nrows):  # Assuming first row is header
            row = sheet.row_values(row_index)
            product_name = row[0]
            product_description = row[1]
            product_price = row[2]
            image_url_or_file = row[3]  # Assuming image URL or file path in fourth column

            # Check if the image is a URL or file path
            if image_url_or_file.startswith('http'):
                image_data = self.download_image_from_url(image_url_or_file)
            else:
                with open(image_url_or_file, 'rb') as image_file:
                    image_data = base64.b64encode(image_file.read())

            # Create or update product
            product = self.search([('name', '=', product_name)], limit=1)
            if product:
                product.write({
                    'description': product_description,
                    'price': product_price,
                    'image': image_data
                })
            else:
                self.create({
                    'name': product_name,
                    'description': product_description,
                    'price': product_price,
                    'image': image_data
                })

    def download_image_from_url(self, image_url):
        try:
            response = requests.get(image_url)
            if response.status_code == 200:
                # Convert image to base64
                image_base64 = base64.b64encode(response.content)
                return image_base64
            else:
                raise ValidationError(_('Failed to download image from URL: %s') % image_url)
        except Exception as e:
            raise ValidationError(_('Error downloading image from URL: %s') % e)
