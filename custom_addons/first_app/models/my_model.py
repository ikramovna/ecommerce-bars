from odoo import models, fields, api


class MyModel(models.Model):
    _name = 'my_model'
    _log_access = True

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')


SEASON = [
    ('winter', 'Winter'),
    ('spring', 'Spring'),
    ('summer', 'Summer'),
    ('autumn', 'Autumn'),
]

AGE = [
    ('babies', 'Babies'),
    ('kid', 'Kid'),
    ('teenagers', 'Teenagers'),
    ('adult', 'Adult'),
]

STATUS = [
    ('pending', 'Pending'),
    ('shipped', 'Shipped'),
    ('delivered', 'Delivered'),
    ('canceled', 'Canceled'),
]

DELIVERY_METHOD = [
    ('take_away', 'Take away'),
    ('deliver', 'Deliver'),
]

RATING = [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
]


class BrandModel(models.Model):
    _name = 'abrand'
    _log_access = True

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')


class CategoryModel(models.Model):
    _name = 'acategory'
    _log_access = True

    name = fields.Char(string='Name', required=True)


class StyleModel(models.Model):
    _name = 'astyle'
    _log_access = True

    name = fields.Char(string='Name', required=True)
    description = fields.Text(string='Description')


class ClothTypeModel(models.Model):
    _name = 'acloth_type'
    _log_access = True

    name = fields.Char(string='Name', required=True)


class LengthModel(models.Model):
    _name = 'alength'
    _log_access = True

    name = fields.Char(string='Name', required=True)


class ColorModel(models.Model):
    _name = 'acolor'
    _log_access = True

    name = fields.Char(string='Name', required=True)
    image = fields.Image(string='Image', required=True)


class SizeModel(models.Model):
    _name = 'asize'
    _log_access = True

    name = fields.Char(string='Name', required=True)


class MaterialModel(models.Model):
    _name = 'amaterial'
    _log_access = True

    name = fields.Char(string='Name', required=True)


class FeatureModel(models.Model):
    _name = 'afeature'
    _log_access = True

    name = fields.Char(string='Name', required=True)


class ProductModel(models.Model):
    _name = 'aproduct'
    _log_access = True

    name = fields.Char(string='Name', required=True)
    description = fields.Html(string='Description')
    weight = fields.Float(string='Weight', required=True)
    min_order = fields.Integer(string='Minimum Order', default=1, required=True)
    season = fields.Selection(string='Season', selection=SEASON, default='winter', required=True)
    age = fields.Selection(string='Age', selection=AGE, default='adult', required=True)
    is_active = fields.Boolean(string='Active', default=True, required=True)
    price = fields.Float(string='Price', required=True)
    remainder = fields.Integer(string='Remainder', default=1, required=True)
    category_id = fields.Many2one('acategory', string='Category', required=True, ondelete='cascade')
    brand_id = fields.Many2one('abrand', string='Brand', required=True, ondelete='cascade')
    style_id = fields.Many2one('astyle', string='Style', ondelete='cascade')
    cloth_type_id = fields.Many2one('acloth_type', string='Cloth', ondelete='cascade')
    length_id = fields.Many2one('alength', string='Length', ondelete='cascade')
    color_id = fields.Many2one('acolor', string='Color', ondelete='cascade')
    size_id = fields.Many2one('asize', string='Size', ondelete='cascade')
    material_id = fields.Many2one('amaterial', string='Material', ondelete='cascade')
    feature_id = fields.Many2one('afeature', string='Feature', ondelete='cascade')


class ImageModel(models.Model):
    _name = 'aimage'
    _log_access = True

    image = fields.Image(string='Image', required=True)
    product_id = fields.Many2one('aproduct', string='Product', required=True, ondelete='cascade')


class PriceModel(models.Model):
    _name = 'aprice'
    _log_access = True

    from_val = fields.Integer(string='From value', required=True)
    to_val = fields.Integer(string='To value')
    price = fields.Float(string='Price', required=True)
    product_id = fields.Many2one('aproduct', string='Product', required=True, ondelete='cascade')


class CartModel(models.Model):
    _name = 'acart'
    _log_access = True

    user_id = fields.Many2one('res.users', string='User', required=True, ondelete='cascade')
    product_id = fields.Many2one('aproduct', string='Product', required=True, ondelete='cascade')
    quantity = fields.Integer(string='Quantity', required=True, default=1)
    summary = fields.Float(string='Summary', required=True)

    @api.onchange("quantity")
    def _onchange_quantity(self):
        for rec in self:
            if rec.quantity == 1:
                rec.summary = rec.product_id.price
            else:
                prices = rec.env['aprice'].search([('product_id', '=', rec.product_id.id)])
                for price_model in prices:
                    if price_model.to_val:
                        if price_model.from_val <= rec.quantity <= price_model.to_val:
                            rec.summary = price_model.price
                    else:
                        if rec.quantity > price_model.to_val:
                            rec.summary = price_model.price


class WishlistModel(models.Model):
    _name = 'awishlist'
    _log_access = True

    user_id = fields.Many2one('res.users', string='User', required=True, ondelete='cascade')
    product_id = fields.Many2one('aproduct', string='Product', required=True, ondelete='cascade')


# class CustomUserModel(models.Model):
#     _name = 'res.users'
#     _inherit = 'res.users'
#
#     full_name = fields.Char(string='Full Name', required=True)
#     phone = fields.Char(string='Phone', required=True)
#     image = fields.Image(string='Image')


class OrderModel(models.Model):
    _name = 'aorder'
    _log_access = True

    product_id = fields.Many2one('aproduct', string='Product', required=True, ondelete='cascade')
    user_id = fields.Many2one('res.users', string='User', required=True, ondelete='cascade')
    address = fields.Char(string='Address', required=True)
    status = fields.Selection(string='Status', selection=STATUS, default='pending', required=True)
    delivery_method = fields.Selection(string='Delivery method', selection=DELIVERY_METHOD, default='take_away',
                                       required=True)
    delivery_time = fields.Datetime(string='Delivery time', required=True, default=fields.Datetime.now)
    delivery_cost = fields.Float(string='Delivery cost', required=True)


class ReviewModel(models.Model):
    _name = 'areview'
    _log_access = True

    rating = fields.Selection(string='Rating', selection=RATING, default='1', required=True)
    comment = fields.Text(string='Comment')
    user_id = fields.Many2one('res.users', string='User', required=True, ondelete='cascade')
    product_id = fields.Many2one('aproduct', string='Product', required=True, ondelete='cascade')
